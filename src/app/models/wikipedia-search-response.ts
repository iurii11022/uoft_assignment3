import { WikipediaSearchResult } from '#src/app/models/wikipedia-search-result';

export type WikipediaSearchResponse = {
  batchcomplete?: string;
  continue?: {
    sroffset: number;
    continue: string;
  };
  query: {
    searchinfo: {
      totalhits: number;
    };
    search: WikipediaSearchResult[];
  };
};
