export type WikipediaSearchParams = {
  action: string;
  format: string;
  list: string;
  utf8: string;
  srsearch: string;
  origin: string;
};
