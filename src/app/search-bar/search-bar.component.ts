import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent {
  public form = new FormControl('', [Validators.required]);

  @Output() public searchValue = new EventEmitter<string>();

  public onFormSubmit(): void {
    if (this.form.valid) {
      this.searchValue.emit(this.form.value as string);
    }
  }
}
