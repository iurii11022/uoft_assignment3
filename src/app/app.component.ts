import { Component } from '@angular/core';
import { WikipediaService } from '#src/app/services/wikipedia.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  public searchResult$ = this.wikipediaService.searchResult$;

  constructor(private wikipediaService: WikipediaService) {}

  public search(searchValue: string): void {
    this.wikipediaService.search(searchValue);
  }
}
