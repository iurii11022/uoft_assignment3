import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  Observable,
  Subject,
  switchMap,
  tap
} from 'rxjs';

import { WikipediaSearchResponse } from '#src/app/models/wikipedia-search-response';
import { WikipediaSearchParams } from '#src/app/models/wikipedia-search-params';
import { WikipediaSearchResult } from '#src/app/models/wikipedia-search-result';

@Injectable()
export class WikipediaService {
  private searchSubject = new Subject<string>();
  public readonly searchResult$ = this.initializeSearch();

  constructor(private http: HttpClient) {}

  public search(searchValue: string): void {
    this.searchSubject.next(searchValue);
  }

  private initializeSearch(): Observable<WikipediaSearchResult[]> {
    return this.searchSubject.pipe(
      map((searchValue: string) => searchValue.trim()),
      debounceTime(500),
      distinctUntilChanged(),
      filter((searchValue: string) => Boolean(searchValue)),
      tap((searchValue: string) => console.log('Searching for:', searchValue)),
      switchMap((searchValue: string) => {
        return this.getApiResponse(searchValue).pipe(
          map((response: WikipediaSearchResponse) => response.query.search)
        );
      })
    );
  }

  private getApiResponse(query: string): Observable<WikipediaSearchResponse> {
    const params: WikipediaSearchParams = {
      action: 'query',
      format: 'json',
      list: 'search',
      utf8: '1',
      srsearch: query,
      origin: '*'
    };

    return this.http.get<WikipediaSearchResponse>(
      'https://en.wikipedia.org/w/api.php',
      { params }
    );
  }
}
