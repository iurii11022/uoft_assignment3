import { Component, Input } from '@angular/core';
import { WikipediaSearchResult } from '#src/app/models/wikipedia-search-result';

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.scss']
})
export class PageListComponent {
  @Input() results!: WikipediaSearchResult[];

  public displayedColumns: string[] = ['title', 'wordcount', 'snippet'];
}
